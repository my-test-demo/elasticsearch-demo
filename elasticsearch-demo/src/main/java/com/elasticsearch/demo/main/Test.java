package com.elasticsearch.demo.main;

import org.apache.commons.lang3.StringUtils;

public class Test {

    private final String MUNICIPALITY = "北京上海重庆天津北京市上海市重庆市天津市";
    private final String FIVE_ZI_ZHI_QU = "新疆内蒙古西藏宁夏广西";
    private final String ZI_ZHI_QU = "自治区";
    private final String PROVINCE = "省";
    private final String CITY = "市";

    /**
     * 组装省市区地址
     *
     * @param address  地址
     * @param provName 省
     * @param cityName 市
     * @param areaName 区
     * @return 如果地址中不包含省市区，则拼接返回，否则直接返回
     */
    public String assemblyAddress(String address, String provName, String cityName, String areaName) {
        if (StringUtils.isEmpty(address)) {
            return address;
        }
        // 去除特殊字符
        address = addrStandard(address);

        //如果省份后面没有带省、自治区、市（直辖市），则拼接上
        if (!StringUtils.isEmpty(provName) && !(provName.contains(CITY) || provName.contains(PROVINCE) || provName.contains(ZI_ZHI_QU))) {
            if (MUNICIPALITY.contains(provName)) {
                provName = provName + CITY;
            } else if (FIVE_ZI_ZHI_QU.contains(provName)) {
                provName = provName + ZI_ZHI_QU;
            } else {
                provName = provName + PROVINCE;
            }
        }
        if (!StringUtils.isEmpty(cityName) && !cityName.contains(CITY)) {
            cityName = cityName + CITY;
        }

        if (address.contains(cityName) && !address.startsWith(provName)) {
            address = address.substring(address.indexOf(provName));
        }
        int num = 0;
        for (int i = 0; i < address.length(); i++) {
            if (address.indexOf(provName, i) != -1) {
                i = address.indexOf(provName, i);
                num++;
            }
        }
        if (address.contains(provName) && num >= 2) {
            address = address.replaceFirst(provName,"");
        }


        StringBuffer sb = new StringBuffer();
        if (!address.contains(provName)) {
            sb.append(provName);
        }
        // 直辖市只拼接省或者市
        if (!address.contains(cityName) && !MUNICIPALITY.contains(provName)) {
            sb.append(cityName);
        }
        if (!address.contains(areaName) && !StringUtils.isEmpty(sb)) {
            sb.append(areaName);
        }

        return StringUtils.isEmpty(sb) ? address : sb.append(address).toString();
    }

    public String addrStandard(String addrName) {
        if (StringUtils.isEmpty(addrName)) {
            return "";
        }
        // 1-去掉非中文、字母、数字字符、大中小书括号、中下划线
        addrName = removeInvalidChar(addrName);

        return addrStandardCommon(addrName);
    }


    /**
     * 数据标准化方法, 不去掉数字
     *
     * @param addrName
     * @return 不为null
     */
    public String addrStandardStayDigit(String addrName) {
        if (StringUtils.isEmpty(addrName)) {
            return "";
        }

        return addrStandardCommon(addrName);
    }

    public String addrStandardCommon(String addrName) {
        if (StringUtils.isEmpty(addrName)) {
            return "";
        }
        // 2-全角转半角
        addrName = full2Half(addrName);
        // 3-繁体转简体
        addrName = gbTraditionToSimple(addrName);
        // 4-去掉自定义无效字符串和xml字符串
        addrName = removeInvalidStr(addrName);
        // 5-对括号进行规范化处理
        addrName = dSSpecialCharReplace(addrName);
        // 6-中英文夹杂的单个字符查找 、对于下划线、中划线等的处理
        addrName = removeInvalidmedLineChar(addrName);
        // 7-如果这个地址串是数字或者英文字母则范围 空字符串
        if (isArabNumber(addrName) || isEnglishChar(addrName)) {
            return "";
        }
        // 8-去掉以中国、地址开头的
        if (StringUtils.isEmpty(addrName) && addrName.startsWith("中国")) {
            addrName = addrName.substring(2, addrName.length());
        }
        // 9-替换不带省市且分词不成功的行政区域
        addrName = replaceProvCity(addrName);
        return addrName;
    }

    /**
     * 1-去掉非中文、字母、数字字符、大中小书括号、中下划线， 以及不以中文开头的字符 尤其是像 𤋮𨦫 这种占用两个Unicode编码(即 单字.length > 1)的字体
     * <p>
     * 在正则表达式前面添加(?i)，表示忽略大小写
     */
    String invalidCharRegex = "[^\u3400-\u9FBBa-zA-Z0-9()[/]{}<>-_ ａ-ｚＡ-ｚ０-９（）［］｛｝＜＞（）【】《》？?]";

    public String removeInvalidChar(String src) {
        if (StringUtils.isEmpty(src)) {
            return src;
        }
        src = src.replaceAll(invalidCharRegex, "");

        int i = 0;
        for (i = 0; i < src.length(); i++) {
            char c = src.charAt(i);
            if (isChinese(c)) {
                break;
            }
        }
        return src.substring(i, src.length());
    }


    /**
     * 2-全角转半角
     * <p>
     * 规则1：全角空格为12288，半角空格为32特殊处理； 规则2：其他字符半角(33-126)与全角(65281-65374)的对应关系是相差65248。
     */
    public String full2Half(String src) {
        if (StringUtils.isEmpty(src)) {
            return src;
        }
        char[] c = src.toCharArray();
        for (int i = 0; i < src.length(); i++) {
            if (c[i] >= 65281 && c[i] <= 65374) {
                c[i] = (char) (c[i] - 65248);
            } else if (c[i] == 12288) {
                // 空格
                c[i] = (char) 32;
            }
        }
        return new String(c);
    }

    /**
     * 3-繁体转简体
     */
    public final String fans =
            "皚藹礙愛骯襖奧壩罷擺敗頒辦絆幫綁鎊謗剝飽寶報鮑輩貝鋇狽備憊繃筆畢斃幣痺閉邊編貶變辯辮標鱉別癟瀕濱賓擯餅並撥缽鉑駁補財採參蠶殘慚慘燦蒼艙倉滄廁側冊測層詫攙摻蟬饞讒纏鏟產闡顫場嘗長償腸廠暢鈔車徹塵陳襯撐稱懲誠騁遲馳恥齒熾沖寵疇躊籌綢櫥廚鋤雛礎儲觸處傳瘡闖創錘純綽辭詞賜聰蔥囪從叢湊躥竄錯達帶貸擔單鄲撣膽憚誕彈當擋黨蕩檔搗島禱導盜燈鄧敵滌遞締顛點墊電澱釣調諜疊釘頂錠訂丟東動棟凍犢獨讀賭鍍鍛斷緞兌隊對噸頓鈍奪墮鵝額訛惡餓兒爾餌貳發罰閥琺礬釩煩販飯訪紡飛誹廢費紛墳奮憤糞豐楓鋒風瘋馮縫諷鳳膚輻撫輔賦復負訃婦縛該鈣蓋趕稈贛岡剛鋼綱崗鎬擱鴿閣鉻個給龔宮鞏貢鉤溝構購夠蠱顧剮關觀館慣貫廣規歸龜閨軌詭櫃貴劊輥滾鍋國過駭韓漢號閡鶴賀橫恆轟鴻紅後壺護滬戶嘩華畫劃話懷壞歡環還緩換喚瘓煥渙黃謊揮輝毀賄穢會燴匯諱誨繪葷渾獲貨禍擊機積飢跡譏雞績緝極輯級擠幾薊劑濟計記際繼紀夾莢頰賈鉀價駕殲監堅箋間艱緘繭檢鹼鹼揀撿簡儉減薦檻鑒踐賤見鍵艦劍餞漸濺澗將漿蔣槳獎講醬膠澆驕嬌攪鉸矯僥腳餃繳絞轎較階節潔結誡屆緊錦僅謹進晉燼盡勁荊莖鯨驚經頸靜鏡徑痙競淨糾廄舊駒舉據鋸懼劇鵑絹覺決訣絕鈞軍駿開凱顆殼課墾懇摳庫褲塊儈寬礦曠況虧巋窺饋潰擴闊蠟臘萊來賴藍欄攔籃闌蘭瀾讕攬覽懶纜爛濫撈勞澇樂鐳壘類淚籬離裡鯉禮麗厲勵礫歷瀝隸倆聯蓮連鐮憐漣斂臉鏈戀煉練糧涼兩輛諒療遼鐐獵臨鄰鱗凜賃齡鈴靈嶺領餾劉龍聾嚨籠壟攏隴樓婁摟簍蘆盧顱廬爐擄鹵虜魯賂祿錄陸驢呂鋁侶屢縷慮濾綠巒攣孿灤亂掄輪倫侖淪綸論蘿羅邏鑼籮騾駱絡媽瑪碼螞馬罵嗎買麥賣邁脈瞞饅蠻滿謾貓錨鉚貿麼沒鎂門悶們錳夢瞇謎彌覓冪綿緬廟滅憫閩鳴銘謬謀畝吶鈉納難撓腦惱鬧餒內擬膩攆釀鳥聶嚙鑷鎳檸獰寧擰濘鈕紐膿濃農瘧諾歐鷗毆嘔漚盤龐拋賠噴鵬騙飄頻貧蘋憑評潑頗鋪譜棲淒臍齊騎豈啟氣棄訖牽鉛遷簽謙錢鉗潛淺譴塹槍嗆牆薔強搶鍬橋喬僑翹竅竊欽親寢輕氫傾頃請慶瓊窮趨區軀驅齲顴權勸卻鵲確讓饒擾繞熱韌認紉榮絨軟銳閏潤薩鰓賽傘喪騷掃澀殺剎紗篩刪閃陝贍繕傷賞燒紹賒攝懾設紳審嬸腎滲聲繩勝聖師獅濕詩時蝕實識駛勢適釋飾視試壽獸樞輸書贖屬術樹豎數帥雙誰稅順說碩爍絲飼鬆聳慫頌訟誦擻蘇訴肅雖隨綏歲孫損筍縮瑣鎖獺撻態攤貪癱灘壇譚談嘆湯燙濤絛討騰謄銻題體屜條貼鐵廳聽烴銅統頭禿圖團頹蛻脫鴕馱駝橢窪襪彎灣頑萬網韋違圍為濰維葦偉偽緯謂衛溫聞紋穩問撾蝸渦窩臥嗚鎢烏誣無蕪吳塢霧務誤錫犧襲習銑戲細蝦轄峽俠狹廈嚇鮮纖賢銜閑顯險現獻縣餡羨憲線廂鑲鄉詳響項蕭囂銷曉嘯蠍協挾攜脅諧寫瀉謝鋅舋興洶鏽繡虛噓須許敘緒續軒懸選癬絢學勛詢尋馴訓訊遜壓鴉鴨啞亞訝閹煙鹽嚴顏閻艷厭硯彥諺驗鴦楊揚瘍陽養樣瑤搖堯遙窯謠藥爺頁業葉醫銥頤遺儀蟻藝億憶義詣議誼譯異繹蔭陰銀飲隱櫻嬰鷹應纓瑩螢營熒蠅贏穎喲擁癰踴詠優憂郵鈾猶誘於輿魚漁娛與嶼語獄譽預馭鴛淵轅園員圓緣遠願約躍鑰粵悅閱雲鄖勻隕運蘊醞暈韻雜災載攢暫贊贓臟鑿棗責擇則澤賊贈軋鍘閘柵詐齋債氈盞斬輾嶄棧佔戰綻張漲帳賬脹趙蟄轍鍺這貞針偵診鎮陣掙睜猙爭幀鄭証織職執隻紙摯擲幟質滯鐘終種腫眾謅軸皺晝驟豬諸誅燭矚囑貯鑄駐專磚轉賺樁裝妝壯狀錐贅墜綴諄著濁茲資漬蹤綜總縱鄒詛組鑽亙睪羋嗇厙厴靨贗匭匱賾剄劌剴傴倀傖佇儕儂儔儼儷俁僨傯僂儻儐儺汆僉糴黌囅鳧兗袞褻臠稟塚訐訌訕謳詎訥詁訶詆詔詒誆誄詿詰詼詵詬詮諍諢詡誚誥誑誒諏諑諉諛諗諂誶諶諫謔謁諤諭諼諳諦諮諞謨讜謖謚謐謫譾譖譙譎讞譫讖巹陘隉鄺鄔鄴郟鄶鄆酈芻奐勱胇堊壙壢壚埡塏塒堝塤薌藶莧萇蓯苧蘢蔦塋煢蕘蓽蕎薈薺犖滎藎蓀葒蒔萵蒞蕕鶯縈蕆蕢蔞驀蘺蕷蘞藺蘄藪蘚奩尷捫摶撟摑摜撳攄榐攖擷擼攛弒嘰嘸囈嚦唄嚀噠嘵嗶噦噲嚌噥嘜嘮嗩嘖囀嘍嚳囁噯轡嚶嚕圇幃幬幘幗嶇峴嵐嶧嶠崢嶗崍嶸崳嶁巔徠獷獪猻獫玀獼餳飩餼飪飫飭飴餉餑餘餛餿饃饈饉饌廡賡廩懺憮慪愾悵愴懌慟懨惻愷惲慳愜慍憒懍閂閆闈閎閔閌闥閭閫鬮閬閾閶閿閽閼闃闋闔闐闕闞灃溈瀧瀘濼涇浹湞澮瀏滸潯淶潿瀆澠瀋淥漵灩灄瀅潷瀠瀟瀲瀦瀨灝騫邇逕邐屨弳嫵嫗媯姍婭嬈孌媧嫻嬋媼嬡嬪嬙嬤駔駟駙騶驛駑駘驍驊駢驪騏騍騅驂騭騖驁騮騸驃驄驏驥驤紆紂紇紈纊紜紕紓紺紲紱縐紼絀絎絳綆綃綈綾綺緋緄綞綬綹綣綰緇緙緗緹緲繢緦緶緱縋緡縉縝縟縞縭縊縑繽縹縵縲繆繅纈繚繒繾繰繯纘璣瑋玨瓏頊璽琿璉璦瓔瓚韙韞韜榪櫪棖樅梟櫛櫳櫨梔櫟檉椏橈楨榿樺檜欒櫺櫝槧欏槨欖櫬櫚櫸檳櫧檣櫫櫓櫞檁歿殤殞殮殫殯軔軛軻轤軹軼軫轢軺軾輊輇輅輒輦輞輟輜輳轆轔戔戧戩甌曇曄暉曖賁貰貺貽贄貲賅贐賑賚賕賧賻覘覬覡覿覦覯覲覷毿氌氬氳牘朧臚冑脛膾腡膃臏歟颮颯颶颼飆轂齏斕煬煒燉燁燜燾檷禎禪懟愨懣戇澩磯碭硨礪礱硤磽磧磣礡龕睞瞼畬羆羈釓釔釙釗釕釷釧釤鍆釵釹鈦鉅鈑鈐鈁鈧鈄鈥鈀鈺鉦鈷鈳鈽鈸鉞鉬鉭鈿鑠鈰鉉鉈鉍鈮鈹鐸銬銠鉺銪鋮鋏鐃鐺銦鎧銖鋌銩鏵銓鉿鎩銚錚銫銃鐋銨銣鐒錸鋱鏗鋰鋯鋨銼鋝鋃鋟鋦錒錆錛錁錕錮錈錟錙鍥鍇鍶鍔鍤鎪鍰鏤鏘鏌鎘鐫鎦鎰鎵鑌鏢鏜鏝鏍鏞鏃鏇鏑鐔鏷鐓鑭鐠鏹鐙鑊鐲鐿鑣鍾穡鳩鳶鴇鴆鴣鶇鸕鴝鴟鷥鴯鷙鴰鵂鸞鵓鸝鵠鵒鷴鵜鵡鵪鵯鶉鶘鶚鷂鶼鸚鷓鷚鷯鷦鷲鷸鷺鸛癤癘癆癇癉瘞癭癮癩癲竇窶襠褳襝襉褸襤皸耬聹聵頇頎頏頡頜頦頷顎顓顳顢顙顥顰虯蟣蠆蜆蠣蟶蛺蟯螄蠐蟈蠑螻蠷罌篤筧籩篳箏簀篋籜簞簫簣籪籟艤艫裊羥糝縶麩趲釅釃鹺躉蹌躒蹺蹕躚躋躓躑躡蹣躕躪躦觴觶靚靂霽靄齔齟齙齠齜齦齬齪齷黽黿鼉雋讎鑾鏨魷魴鱍鱸穌鮒鱟鮐鮚鮪鮞鱭鮫鯗鱘鯁鱺鰱鰹鰣鰷鯀鯊鯇鯽鯖鯪鯫鯡鯤鯧鯢鯰鯛鯔鰈鱷鰍鰒鰉鰲鰭鰨鰥鰩鰳鰾鱈鰻鱖鱔鱒鱧韃韉鶻髏髖髕魘魎饗饜鬢麼黷黲鼴遊㑳㠏㩜䊷䋙䋻䝼䯀䰾䱷䱽䲁佈併係俔倈倖傑傢傭僕僞僱儘儸兇凈剋剗剷劏劚勩勳卽厠厤叄咼唚啓啢喎喫嗊嘽噁噚噝噹嚐嚥嚮嚲囌垵埰堖塗墰墻壋壜壠壼奬奼姦嫿嬀寀屍屓崑崙崬嵗嶔嶢嶮嶴嶽巰幹幺廝弔彆彙彞悞慄慤慼慾懞戱挩挱捨捲掗掛搵撏撝撲擓旂旣昇暘曆曏曨曬朮杴桿梘梲棊棡棶榦榲槓槤樑樸檟檮檯櫱欅欞歎殨殭殰殻氂汙泝洩淩湧準滙滷滻潁潙澐澦澾瀇瀰灑灕灠灧煱熅熗熲燬燶爲獁獃獮獱瑒瑲璫甕産甦畵痠痾瘂瘮瘲瘺瘻癒癡癢癥癧皰盃盪眥睏瞘瞜瞭瞶矇矓硃硜碕碸磑礆祘禕禡禦禰秈稏稜穀穠穫穭窵竈竪筴箇範築篔簑簹簾籙籤籲糰糲糶糹紖紘紝紬紮紵紿絅絝絰絶綉綌綐綫綯綳綵緑緓緔緻縕縗縧縳縴繐繫繮繸繿纍纔罈罎耮脣腖臢臺莊菴葤葯蒓蓴蔔蔘蕁蕒蕓薀薑薘薟薳薴藴虆蝟螮螿蟎蟲蠨衆衊衕衚衝衹裏製複褌褘襆襇襏襬覎覥訁訒託訢訩註詖詗詘誇誌説誾諝謡謭譅證譭譸讚豔豶貙貟賙賫賰賵贇贋贔贜赬躂軑軤軲輈輓輬轀迴週遡酇醖醜醱釁釐釒釺釾鈃鈈鈎鈒鉋鉕鉢鉶銍銛銱銹鋙鋣鋥鋩鋭鋶錇錏錡錩録錶鍀鍁鍃鍈鍊鍚鍠鍩鎄鎇鎌鎔鎚鎛鎡鎣鎲鎸鎿鏐鏚鏰鏺鐝鐥鐦鐧鐨鐯鐶鑑鑔鑕鑞鑥鑱鑹钁镟閈閒閤閲闆闍闒闓闠闢闤陞霢靦鞀鞝鞦鞽韁韆韍韝頮頲頴頽顒顔顬颭颱颳颸颺颻飀飈飣飥飱飿餄餎餏餕餖餚餜餱餵餶餷餺饁饊饑饢馹駡駰駸騂騌騤騧驌驦驫髒髮鬍鬚鬥鬨鬩鬱魛魢魨魺鮁鮃鮊鮋鮍鮎鮓鮜鮝鮣鮦鮭鮳鮶鮺鯒鯕鯝鯴鯷鯿鰁鰂鰃鰆鰌鰏鰐鰜鰟鰠鰧鰮鰵鰺鰼鱂鱅鱝鱠鱣鱤鱨鱯鳬鳲鳾鴒鴞鴴鴷鵁鵃鵐鵮鵰鵷鵾鶄鶊鶓鶖鶡鶥鶩鶪鶬鶲鶹鶺鶿鷀鷁鷄鷈鷊鷖鷫鷳鷽鷿鸂鸇鸌鸏鸘鹹麪麫麯麵麽黴黶鼕齇齎齕齗齣齶龎靑籣";
    public final String jian =
            "皑蔼碍爱肮袄奥坝罢摆败颁办绊帮绑镑谤剥饱宝报鲍辈贝钡狈备惫绷笔毕毙币痹闭边编贬变辩辫标鳖别瘪濒滨宾摈饼并拨钵铂驳补财采参蚕残惭惨灿苍舱仓沧厕侧册测层诧搀掺蝉馋谗缠铲产阐颤场尝长偿肠厂畅钞车彻尘陈衬撑称惩诚骋迟驰耻齿炽冲宠畴踌筹绸橱厨锄雏础储触处传疮闯创锤纯绰辞词赐聪葱囱从丛凑蹿窜错达带贷担单郸掸胆惮诞弹当挡党荡档捣岛祷导盗灯邓敌涤递缔颠点垫电淀钓调谍叠钉顶锭订丢东动栋冻犊独读赌镀锻断缎兑队对吨顿钝夺堕鹅额讹恶饿儿尔饵贰发罚阀珐矾钒烦贩饭访纺飞诽废费纷坟奋愤粪丰枫锋风疯冯缝讽凤肤辐抚辅赋复负讣妇缚该钙盖赶秆赣冈刚钢纲岗镐搁鸽阁铬个给龚宫巩贡钩沟构购够蛊顾剐关观馆惯贯广规归龟闺轨诡柜贵刽辊滚锅国过骇韩汉号阂鹤贺横恒轰鸿红后壶护沪户哗华画划话怀坏欢环还缓换唤痪焕涣黄谎挥辉毁贿秽会烩汇讳诲绘荤浑获货祸击机积饥迹讥鸡绩缉极辑级挤几蓟剂济计记际继纪夹荚颊贾钾价驾歼监坚笺间艰缄茧检碱硷拣捡简俭减荐槛鉴践贱见键舰剑饯渐溅涧将浆蒋桨奖讲酱胶浇骄娇搅铰矫侥脚饺缴绞轿较阶节洁结诫届紧锦仅谨进晋烬尽劲荆茎鲸惊经颈静镜径痉竞净纠厩旧驹举据锯惧剧鹃绢觉决诀绝钧军骏开凯颗壳课垦恳抠库裤块侩宽矿旷况亏岿窥馈溃扩阔蜡腊莱来赖蓝栏拦篮阑兰澜谰揽览懒缆烂滥捞劳涝乐镭垒类泪篱离里鲤礼丽厉励砾历沥隶俩联莲连镰怜涟敛脸链恋炼练粮凉两辆谅疗辽镣猎临邻鳞凛赁龄铃灵岭领馏刘龙聋咙笼垄拢陇楼娄搂篓芦卢颅庐炉掳卤虏鲁赂禄录陆驴吕铝侣屡缕虑滤绿峦挛孪滦乱抡轮伦仑沦纶论萝罗逻锣箩骡骆络妈玛码蚂马骂吗买麦卖迈脉瞒馒蛮满谩猫锚铆贸么没镁门闷们锰梦眯谜弥觅幂绵缅庙灭悯闽鸣铭谬谋亩呐钠纳难挠脑恼闹馁内拟腻撵酿鸟聂啮镊镍柠狞宁拧泞钮纽脓浓农疟诺欧鸥殴呕沤盘庞抛赔喷鹏骗飘频贫苹凭评泼颇铺谱栖凄脐齐骑岂启气弃讫牵铅迁签谦钱钳潜浅谴堑枪呛墙蔷强抢锹桥乔侨翘窍窃钦亲寝轻氢倾顷请庆琼穷趋区躯驱龋颧权劝却鹊确让饶扰绕热韧认纫荣绒软锐闰润萨鳃赛伞丧骚扫涩杀刹纱筛删闪陕赡缮伤赏烧绍赊摄慑设绅审婶肾渗声绳胜圣师狮湿诗时蚀实识驶势适释饰视试寿兽枢输书赎属术树竖数帅双谁税顺说硕烁丝饲松耸怂颂讼诵擞苏诉肃虽随绥岁孙损笋缩琐锁獭挞态摊贪瘫滩坛谭谈叹汤烫涛绦讨腾誊锑题体屉条贴铁厅听烃铜统头秃图团颓蜕脱鸵驮驼椭洼袜弯湾顽万网韦违围为潍维苇伟伪纬谓卫温闻纹稳问挝蜗涡窝卧呜钨乌诬无芜吴坞雾务误锡牺袭习铣戏细虾辖峡侠狭厦吓鲜纤贤衔闲显险现献县馅羡宪线厢镶乡详响项萧嚣销晓啸蝎协挟携胁谐写泻谢锌衅兴汹锈绣虚嘘须许叙绪续轩悬选癣绚学勋询寻驯训讯逊压鸦鸭哑亚讶阉烟盐严颜阎艳厌砚彦谚验鸯杨扬疡阳养样瑶摇尧遥窑谣药爷页业叶医铱颐遗仪蚁艺亿忆义诣议谊译异绎荫阴银饮隐樱婴鹰应缨莹萤营荧蝇赢颖哟拥痈踊咏优忧邮铀犹诱于舆鱼渔娱与屿语狱誉预驭鸳渊辕园员圆缘远愿约跃钥粤悦阅云郧匀陨运蕴酝晕韵杂灾载攒暂赞赃脏凿枣责择则泽贼赠轧铡闸栅诈斋债毡盏斩辗崭栈占战绽张涨帐账胀赵蛰辙锗这贞针侦诊镇阵挣睁狰争帧郑证织职执只纸挚掷帜质滞钟终种肿众诌轴皱昼骤猪诸诛烛瞩嘱贮铸驻专砖转赚桩装妆壮状锥赘坠缀谆着浊兹资渍踪综总纵邹诅组钻亘睾芈啬厍厣靥赝匦匮赜刭刿剀伛伥伧伫侪侬俦俨俪俣偾偬偻傥傧傩氽佥籴黉冁凫兖衮亵脔禀冢讦讧讪讴讵讷诂诃诋诏诒诓诔诖诘诙诜诟诠诤诨诩诮诰诳诶诹诼诿谀谂谄谇谌谏谑谒谔谕谖谙谛谘谝谟谠谡谥谧谪谫谮谯谲谳谵谶卺陉陧邝邬邺郏郐郓郦刍奂劢巯垩圹坜垆垭垲埘埚埙芗苈苋苌苁苎茏茑茔茕荛荜荞荟荠荦荥荩荪荭莳莴莅莸莺萦蒇蒉蒌蓦蓠蓣蔹蔺蕲薮藓奁尴扪抟挢掴掼揿摅搌撄撷撸撺弑叽呒呓呖呗咛哒哓哔哕哙哜哝唛唠唢啧啭喽喾嗫嗳辔嘤噜囵帏帱帻帼岖岘岚峄峤峥崂崃嵘嵛嵝巅徕犷狯狲猃猡猕饧饨饩饪饫饬饴饷饽馀馄馊馍馐馑馔庑赓廪忏怃怄忾怅怆怿恸恹恻恺恽悭惬愠愦懔闩闫闱闳闵闶闼闾阃阄阆阈阊阌阍阏阒阕阖阗阙阚沣沩泷泸泺泾浃浈浍浏浒浔涞涠渎渑渖渌溆滟滠滢滗潆潇潋潴濑灏骞迩迳逦屦弪妩妪妫姗娅娆娈娲娴婵媪嫒嫔嫱嬷驵驷驸驺驿驽骀骁骅骈骊骐骒骓骖骘骛骜骝骟骠骢骣骥骧纡纣纥纨纩纭纰纾绀绁绂绉绋绌绗绛绠绡绨绫绮绯绲缍绶绺绻绾缁缂缃缇缈缋缌缏缑缒缗缙缜缛缟缡缢缣缤缥缦缧缪缫缬缭缯缱缲缳缵玑玮珏珑顼玺珲琏瑷璎瓒韪韫韬杩枥枨枞枭栉栊栌栀栎柽桠桡桢桤桦桧栾棂椟椠椤椁榄榇榈榉槟槠樯橥橹橼檩殁殇殒殓殚殡轫轭轲轳轵轶轸轹轺轼轾辁辂辄辇辋辍辎辏辘辚戋戗戬瓯昙晔晖暧贲贳贶贻贽赀赅赆赈赉赇赕赙觇觊觋觌觎觏觐觑毵氇氩氲牍胧胪胄胫脍脶腽膑欤飑飒飓飕飙毂齑斓炀炜炖烨焖焘祢祯禅怼悫懑戆泶矶砀砗砺砻硖硗碛碜礴龛睐睑畲罴羁钆钇钋钊钌钍钏钐钔钗钕钛钜钣钤钫钪钭钬钯钰钲钴钶钸钹钺钼钽钿铄铈铉铊铋铌铍铎铐铑铒铕铖铗铙铛铟铠铢铤铥铧铨铪铩铫铮铯铳铴铵铷铹铼铽铿锂锆锇锉锊锒锓锔锕锖锛锞锟锢锩锬锱锲锴锶锷锸锼锾镂锵镆镉镌镏镒镓镔镖镗镘镙镛镞镟镝镡镤镦镧镨镪镫镬镯镱镳锺穑鸠鸢鸨鸩鸪鸫鸬鸲鸱鸶鸸鸷鸹鸺鸾鹁鹂鹄鹆鹇鹈鹉鹌鹎鹑鹕鹗鹞鹣鹦鹧鹨鹩鹪鹫鹬鹭鹳疖疠痨痫瘅瘗瘿瘾癞癫窦窭裆裢裣裥褛褴皲耧聍聩顸颀颃颉颌颏颔颚颛颞颟颡颢颦虬虮虿蚬蛎蛏蛱蛲蛳蛴蝈蝾蝼蠼罂笃笕笾筚筝箦箧箨箪箫篑簖籁舣舻袅羟糁絷麸趱酽酾鹾趸跄跞跷跸跹跻踬踯蹑蹒蹰躏躜觞觯靓雳霁霭龀龃龅龆龇龈龉龊龌黾鼋鼍隽雠銮錾鱿鲂鲅鲈稣鲋鲎鲐鲒鲔鲕鲚鲛鲞鲟鲠鲡鲢鲣鲥鲦鲧鲨鲩鲫鲭鲮鲰鲱鲲鲳鲵鲶鲷鲻鲽鳄鳅鳆鳇鳌鳍鳎鳏鳐鳓鳔鳕鳗鳜鳝鳟鳢鞑鞯鹘髅髋髌魇魉飨餍鬓麽黩黪鼹游㑇㟆㨫䌶䌺䌾䞍䯅鲃䲣䲝鳚布并系伣俫幸杰家佣仆伪雇尽㑩凶净克刬铲㓥㔉勚勋即厕历叁呙吣启唡㖞吃唝啴恶㖊咝当尝咽向亸苏埯采垴涂坛墙垱坛垅壸奖姹奸婳妫采尸屃昆仑岽岁嵚峣崄岙岳巯干么厮吊别汇彝悮栗悫戚欲蒙戯捝挲舍卷挜挂揾挦㧑扑㧟旗既升旸历向昽晒术锨杆枧棁棋梾干榅杠梿梁朴槚梼台蘖榉棂叹㱮僵㱩壳牦污溯泄凌涌准汇卤浐颍沩沄滪㳠㲿弥洒漓漤滟㶽煴炝颎毁㶶为犸呆狝㺍玚玱珰瓮产苏画酸疴痖瘆疭瘘瘘愈痴痒症疬疱杯荡眦困眍䁖了瞆蒙眬朱硁埼砜硙硷算祎祃御祢籼䅉棱谷秾获稆窎灶竖䇲个范筑筼蓑筜帘箓签吁团粝粜纟纼纮纴䌷扎纻绐䌹绔绖绝绣绤䌼线绹绷彩绿绬绱致缊缞绦䌸纤穗系缰䍁䍀累才坛坛耢唇胨臜台庄庵荮药莼莼卜参荨荬芸蕰姜荙莶䓕苧蕴蔂猬䗖螀螨虫蟏众蔑同胡冲只里制复裈袆幞裥袯䙓觃觍讠讱托䜣讻注诐诇诎夸志说訚谞谣谫䜧证毁诪赞艳豮䝙贠赒赍䞐赗赟赝赑赃赪跶轪轷轱辀挽辌辒回周溯酂酝丑酦衅厘钅钎䥺钘钚钩钑铇钷钵铏铚铦铞锈铻铘锃铓锐锍锫铔锜锠录表锝锨锪锳炼钖锽锘锿镅镰镕锤镈镃蓥镋镌镎镠戚镚䥽镢䦅锎锏镄䦃镮鉴镲锧镴镥镵镩镢旋闬闲合阅板阇阘闿阓辟阛升霡腼鼗绱秋鞒缰千韨鞲颒颋颕颓颙颜颥飐台刮飔飏飖飗飚饤饦飧饳饸饹饻馂饾肴馃糇喂馉馇馎馌馓饥馕驲骂骃骎骍骔骙䯄骕骦骉脏发胡须斗哄阋郁鱽鱾鲀鲄鲅鲆鲌鲉鲏鲇鲊鲘鲞䲟鲖鲑鲓鲪鲝鲬鲯鲴鲺鳀鳊鳈鲗鳂䲠䲡鲾鳄鳒鳑鳋䲢鳁鳘鲹鳛鳉鳙鲼鲙鳣鳡鲿鳠凫鸤䴓鸰鸮鸻䴕䴔鸼鹀鹐雕鹓鹍䴖鹒鹋鹙鹖鹛鹜䴗鸧鹟鹠鹡鹚鹚鹢鸡䴘鹝鹥鹔鹇鸴䴙㶉鹯鹱鹲鹴咸面面曲面么霉黡冬齄赍龁龂出腭厐青兰";

    public String gbTraditionToSimple(String src) {
        if (StringUtils.isEmpty(src)) {
            return src;
        }
        for (int i = 0; i < src.length(); i++) {
            int index = fans.indexOf(src.charAt(i));
            if (index != -1) {
                src = src.replace(src.charAt(i), jian.charAt(index));
            }
        }
        return src;
    }

    /**
     * 4-去掉自定义无效字符串和xml字符串
     */
    public final String[] invalidStr = {"amp", "lt", "gt", "xd", "NA", "acute", "mdash", "apos", "middot", "hellip",
            "nbsp", "ldquo", "lsquo", "quot", "rdquo", "CDATA", "null", "ocirc", "sub", "larr", "raquo", "sum", "frac34",
            "sup", "euml", "sect", "Uuml", "empty", "image", "chi", "rArr", "shy", "perp", "psi", "harr", "zwj", "not",
            "uml", "sim", "alpha", "Lambda", "bdquo", "ge", "zeta", "yen", "xi", "rho", "laquo", "exist", "lceil", "darr",
            "loz", "prop", "copy", "phi", "rlm", "le", "yuml", "cent", "there4", "Gamma", "sup2", "sup3", "eta", "sup1",
            "eth", "uArr", "piv", "lrm", "mu", "tau", "real", "ne", "ni", "part", "cap", "and", "ang", "lang", "or", "Nu",
            "int", "pi", "reg"};
    public final String[] invalidCNStr = {"｀", "。", "＾", ",", ":", "&", "=", "%", ";", "<[[", "]]>", "\b", "\t", "\n",
            "\f", "\r", "\'", "\"", "\\", "\\+", "\\*", "\\.", "\\/", "\\|", "\\^", "\\?", "[a-z]*\\ ", "[　*|*|*|//s*]*",
            "省直辖县级行政区划", "送货至", "自治区直辖县级行政区划"};

    public String removeInvalidStr(String src) {
        if (StringUtils.isEmpty(src)) {
            return src;
        }
        for (String str : invalidStr) {
            src = src.replaceAll("(?i)" + str, "");
        }
        for (String str : invalidCNStr) {
            try {
                src = src.replace(str, "");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return src;
    }

    /**
     * 5-对括号进行规范化处理 对括号进行规范化处理 括号的体会， 这里只能处理转换为半角后的字符 其中括号的种类可以人物扩充
     */
    public final String[] leftBracketArray = {"<", "[", "{", "《", "(", "（"};
    public final String[] rightBracketArray = {">", "]", "}", "》", ")", "）"};

    public String dSSpecialCharReplace(String src) {
        if (StringUtils.isEmpty(src)) {
            return src;
        }
        for (int i = 0; i < leftBracketArray.length; i++) {
            src = src.replace(leftBracketArray[i], "");
        }
        for (int i = 0; i < rightBracketArray.length; i++) {
            src = src.replace(rightBracketArray[i], "");
        }
        return src;
    }

    /**
     * 6-对于下划线、中划线等的处理
     */
    public final String[] line = {"_", "-"};

    /**
     * @return java.lang.String
     * @Description 删掉特殊字符
     * @Date 19:58 2019/6/21
     * @Param [src]
     **/
    public String removeInvalidmedLineChar(String src) {
        if (StringUtils.isEmpty(src)) {
            return src;
        }
        for (int i = 0; i < line.length; i++) {
            // 去掉连续出现多次的中\下化划线
            src = src.replaceAll(line[i] + "{2,}", line[i]);
            char chars[] = src.toCharArray();
            char lineChar = line[i].charAt(0);
            for (int j = 0; j < chars.length; j++) {
                // 字符是中\下划线
                if (chars[j] == lineChar) {
                    // 第一个字或者最后一个是中\下划线，直接去掉
                    if (j == 0 || j == chars.length - 1) {
                        chars[j] = ' ';
                    } else {
                        String berfor = String.valueOf(chars[j - 1]);
                        String next = String.valueOf(chars[j + 1]);
                        // 中\下划线在字符串中间的情况。 只有中\下划线的前后都是数字\字母的，保留成中划线
                        if ((isArabNumber(berfor) && isArabNumber(next))
                                || (isEnglishChar(berfor) && isEnglishChar(next))) {
                            chars[j] = '-';
                        } else {
                            chars[j] = ' ';
                        }
                    }
                }
            }
            // 去掉空格
            src = new String(chars).replace(" ", "");
        }
        return src;
    }

    /**
     * 9-替换不带省市且分词不成功的行政区域
     */
    public final String[] provCitys = {"北京北京", "北京北京市", "河北石家庄", "河北石家庄市", "河北唐山", "河北唐山市", "河北张家口", "河北张家口市", "河南新乡",
            "河南新乡市", "河南三门峡", "河南三门峡市", "广东河源", "广东河源市"};
    public final String[] stdProvCitys = {"北京北京市", "北京北京市", "河北省石家庄市", "河北省石家庄市", "河北省唐山市", "河北省唐山市", "河北省张家口市",
            "河北省张家口市", "河南省新乡市", "河南省新乡市", "河南省三门峡市", "河南省三门峡市", "广东省河源市", "广东省河源市"};

    public String replaceProvCity(String src) {
        if (StringUtils.isEmpty(src)) {
            return src;
        }
        for (int i = 0; i < provCitys.length; i++) {
            String tmp = provCitys[i];
            if (!StringUtils.isEmpty(tmp) && src.startsWith(tmp)) {
//                src = stdProvCitys[i] + src.substring(tmp.length(), src.length());
                src = src.substring(tmp.length(), src.length());
                System.out.println("src:"+src);
                if (src.startsWith("省")) {
                   src = src.replace("省","");
                }
                if (src.startsWith("市")){
                    src = src.replace("市","");
                }
                src = stdProvCitys[i] +src;
            }
        }
        return src;
    }

    /**
     * @param prov 输入省份
     * @return prov  输出全称
     * @Description 省分简单标准化过程
     * @date 19:56 2019/6/21
     **/
    public String provStd(String prov) {
        if (StringUtils.isEmpty(prov)) {
            return "";
        }
        if (prov.contains("广西")) {
            return "广西壮族自治区";
        }
        if (prov.contains("内蒙")) {
            return "内蒙古自治区";
        }
        if (prov.contains("西藏")) {
            return "西藏自治区";
        }
        if (prov.contains("澳")) {
            return "澳门特别行政区";
        }
        if (prov.contains("宁夏")) {
            return "宁夏回族自治区";
        }
        if (prov.contains("台湾")) {
            return "台湾";
        }
        if (prov.contains("香港")) {
            return "香港特别行政区";
        }
        if (prov.contains("新疆")) {
            return "新疆维吾尔自治区";
        }
        if (prov.contains("黑龙")) {
            return "黑龙江省";
        }
        if (prov.contains("安徽")) {
            return "安徽省";
        }
        if (prov.contains("福建")) {
            return "福建省";
        }
        if (prov.contains("河北")) {
            return "河北省";
        }
        if (prov.contains("四川")) {
            return "四川省";
        }
        if (prov.contains("甘肃")) {
            return "甘肃省";
        }
        if (prov.contains("云南")) {
            return "云南省";
        }
        if (prov.contains("广东")) {
            return "广东省";
        }
        if (prov.contains("贵州")) {
            return "贵州省";
        }
        if (prov.contains("陕西")) {
            return "陕西省";
        }
        if (prov.contains("海南")) {
            return "海南省";
        }
        if (prov.contains("江苏")) {
            return "江苏省";
        }
        if (prov.contains("浙江")) {
            return "浙江省";
        }
        if (prov.contains("湖南")) {
            return "湖南省";
        }
        if (prov.contains("湖北")) {
            return "湖北省";
        }
        if (prov.contains("河南")) {
            return "河南省";
        }
        if (prov.contains("江西")) {
            return "江西省";
        }
        if (prov.contains("山东")) {
            return "山东省";
        }
        if (prov.contains("辽宁")) {
            return "辽宁省";
        }
        if (prov.contains("青海")) {
            return "青海省";
        }
        if (prov.contains("山西")) {
            return "山西省";
        }
        if (prov.contains("吉林")) {
            return "吉林省";
        }
        if (prov.contains("上海")) {
            return "上海";
        }
        if (prov.contains("天津")) {
            return "天津";
        }
        if (prov.contains("重庆")) {
            return "重庆";
        }
        if (prov.contains("北京")) {
            return "北京";
        }
        return "";
    }

    /**
     * 半角转全角
     */
    public String half2Full(String src) {
        if (StringUtils.isEmpty(src)) {
            return src;
        }
        char[] c = src.toCharArray();
        for (int i = 0; i < c.length; i++) {
            if (c[i] == 32) {
                c[i] = (char) 12288;
            } else if (c[i] < 127) {
                c[i] = (char) (c[i] + 65248);
            }
        }
        return new String(c);
    }

    /**
     * 判断整个字符串是否都是阿拉伯数字
     */
    public boolean isArabNumber(String str) {
        if (str == null) {
            return false;
        }
        return str.matches("[0-9]+");
    }

    /**
     * 判断字符串是否全部是英文字符
     */
    public boolean isEnglishChar(String str) {
        if (str == null) {
            return false;
        }
        return str.matches("[a-zA-Z]+");
    }

    /**
     * 判断字符是否是CJK字符 Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS ： 4E00-9FBF：CJK 统一表意符号
     * Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS ：F900-FAFF：CJK 兼容象形文字
     * Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A ：3400-4DBF：CJK 统一表意符号扩展 A
     * CJK的意思是“Chinese，Japanese，Korea”的简写 ，实际上就是指中日韩三国的象形文字的Unicode编码 Character.UnicodeBlock.GENERAL_PUNCTUATION
     * ：2000-206F：常用标点 Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION ：3000-303F：CJK 符号和标点
     * Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS ：FF00-FFEF：半角及全角形式 Character.isLetter(c)：判断字符是否是字母
     * Character.isDigit(c)：判断字符是否是数字
     */
    public boolean isChinese(char c) {
        Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
        if (ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS
                || ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS
                || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A
                || ub == Character.UnicodeBlock.GENERAL_PUNCTUATION
                || ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION
                || ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS) {
            return true;
        }
        return false;
    }

    public String getSubProvince(String province) {
        return province.replace("省", "").replace("市", "").replace("自治区", "");
    }

    public String getSubCity(String city) {
        return city.replace("市", "");
    }

    //区，县，县级市
    public String getSubCounty(String county) {
        return county.replace("区", "").replace("县", "").replace("市", "");
    }


    public static void main(String[] args) {
        Test test = new Test();
        String address = "上海市上海市松江区方松街道新松江路2500弄蓝色剑桥175号";
        String province = "上海市";
        String city = "上海市";
        String county = "松江区";
        System.out.println("    原地址："+address);
        Long s = System.currentTimeMillis();
        String stdAddress = test.assemblyAddress(address,province,city,county);
        Long e = System.currentTimeMillis();
        System.out.println("格式化地址："+stdAddress);
        System.out.println("cost time:"+(e-s));
    }
}
