package com.elasticsearch.demo.service;

import com.elasticsearch.demo.entity.User;

import java.io.IOException;
import java.util.List;

/**
 * @Interface EsService
 * @Description TODO
 * @Author lyk
 * @Date 2019/4/23 13:35
 * @Version 1.0
 **/

public interface EsService {
    void createIndex(String index);
    boolean existsIndex(String index) throws IOException;
    void add(String index, String type, User user);
    List<User> get();
}
