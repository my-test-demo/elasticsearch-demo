package com.elasticsearch.demo.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.elasticsearch.demo.entity.User;
import com.elasticsearch.demo.service.EsService;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.get.GetIndexRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName EsServiceImpl
 * @Description TODO
 * @Author lyk
 * @Date 2019/4/23 13:36
 * @Version 1.0
 **/
@Service
@Slf4j
public class EsServiceImpl implements EsService {

    @Qualifier("esClient")
    @Autowired
    private RestHighLevelClient esClient;

    //Relational DB -> Databases -> Tables -> Rows -> Columns
    //Elasticsearch -> Indices -> Types -> Documents -> Fields

    @Override
    public void createIndex(String index) {
        IndexRequest indexRequest = new IndexRequest();
        try {
            CreateIndexRequest createIndexRequest = new CreateIndexRequest(index);
            CreateIndexResponse createIndexResponse = esClient.indices().create(createIndexRequest, RequestOptions.DEFAULT);
            log.info("createIndex：" + JSONObject.toJSONString(createIndexRequest));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean existsIndex(String index) throws IOException {
        GetIndexRequest request = new GetIndexRequest();
        request.indices(index);
        boolean exists = esClient.indices().exists(request, RequestOptions.DEFAULT);
        log.info("index " + index + " isExists：" + exists);
        return exists;

    }

    @Override
    public void add(String index, String type, User user) {
        try {
            if (existsIndex(index)) {
                IndexRequest indexRequest = new IndexRequest(index, type, String.valueOf(user.getUserId()));
                indexRequest.source(JSON.toJSONString(user), XContentType.JSON);
                IndexResponse indexResponse = esClient.index(indexRequest, RequestOptions.DEFAULT);
                log.info("add："+JSONObject.toJSONString(indexResponse));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<User> get(){
        SearchRequest searchRequest = new SearchRequest( "test_1");
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        BoolQueryBuilder bq =  QueryBuilders.boolQuery();
        sourceBuilder.query(bq);
        searchRequest.source(sourceBuilder);
        SearchResponse searchResponse = null;
        try {
            searchResponse = esClient.search(searchRequest,RequestOptions.DEFAULT);
        } catch (final Exception e) {
            log.error("获取返回数据发生异常：{}", e.getMessage());
        }
        SearchHits hits = searchResponse.getHits();
        Map<String,Object> map = new HashMap<>();
        List<User> users = new ArrayList<>();
        if (null != searchResponse) {
            SearchHit[] searchHits = hits.getHits();

            for (SearchHit hit : searchHits) {
                map = hit.getSourceAsMap();
                log.info("map:{}",map);
            }
        }
        return users;
    }
}
