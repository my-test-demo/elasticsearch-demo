package com.elasticsearch.demo.config;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.util.ArrayList;

/**
 * @ClassName ElasticsearchConfig
 * @Description TODO
 * @Author lyk
 * @Date 2019/4/23 10:54
 * @Version 1.0
 **/
@Configuration
@Slf4j
public class ElasticsearchConfig {
    /**
     * 集群地址，多个用,隔开
     */
    @Value("${es.hosts}")
    private String hosts;
    /**
     * 端口号
     */
    @Value("${es.port}")
    private int port;
    /**
     * 使用的协议
     */
    private String scheme = "http";
    private ArrayList<HttpHost> hostList = null;
    /**
     * 建立连接超时时间
     */
    @Value("${es.connectTimeOut}")
    private int connectTimeOut;
    /**
     * 获取数据的超时时间
     */
    @Value("${es.socketTimeOut}")
    private int socketTimeOut;
    /**
     * 连接池获取到连接的超时时间
     */
    @Value("${es.requestTimeOut}")
    private int requestTimeOut;
    /**
     * 最大连接数
     */
    @Value("${es.maxConnectNum}")
    private int maxConnectNum;
    /**
     * 最大路由连接数
     */
    @Value("${es.maxConnectPerRoute}")
    private int maxConnectPerRoute;

    private RestHighLevelClient client;

    @Bean(name = "esClient")
    public RestHighLevelClient client() {
        hostList = new ArrayList<>();
        String[] hostStr = hosts.split(",");
        for (String host : hostStr){
            hostList.add(new HttpHost(host, port, scheme));
        }
        RestClientBuilder builder = RestClient.builder(hostList.toArray(new HttpHost[0]));
        // 异步httpclient连接延时配置
        builder.setRequestConfigCallback(requestConfigBuilder -> {
            requestConfigBuilder.setConnectTimeout(connectTimeOut);
            requestConfigBuilder.setSocketTimeout(socketTimeOut);
            requestConfigBuilder.setConnectionRequestTimeout(requestTimeOut);
            return requestConfigBuilder;
        });
        // 异步httpclient连接数配置
        builder.setHttpClientConfigCallback(httpClientBuilder -> {
            httpClientBuilder.setMaxConnTotal(maxConnectNum);
            httpClientBuilder.setMaxConnPerRoute(maxConnectPerRoute);
            return httpClientBuilder;
        });
        RestHighLevelClient client = new RestHighLevelClient(builder);
        log.info("连接成功："+ JSONObject.toJSONString(client));
        return client;
    }

    /**
     * 关闭连接
     */
    public void close() {
        if (client != null) {
            try {
                client.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


}
