package com.elasticsearch.demo.controller;

import com.elasticsearch.demo.entity.User;
import com.elasticsearch.demo.service.EsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

/**
 * @ClassName EsController
 * @Description TODO
 * @Author lyk
 * @Date 2019/4/23 13:42
 * @Version 1.0
 **/
@RestController
public class EsController {

    @Autowired
    private EsService esService;

    @PostMapping("/createIndex")
    public void createIndex(String index) {
        esService.createIndex(index);
    }

    @PostMapping("/existsIndex")
    public boolean exitIndex(String index) {
        boolean exists = false;
        try {
            exists = esService.existsIndex(index);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return exists;
    }

    @PostMapping("/add")
    public void add(String index, String type) {
        User user = new User();
        user.setUserId(2);
        user.setUserName("Jack");
        user.setEmail("1234jack@126.com");
        user.setPhone("15511112203");
        user.setUserType(0);
        user.setUserStatus(0);
        user.setCreateDate(LocalDate.now());
        esService.add(index, type, user);
    }

    @GetMapping("/get")
    public List<User> getUser(){
       return esService.get();
    }
}
